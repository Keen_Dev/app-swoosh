//
//  Player.swift
//  app-swoosh
//
//  Created by Craig Keen on 8/13/17.
//  Copyright © 2017 Craig Keen. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String!
    var selectedSkillLevel: String!
}
